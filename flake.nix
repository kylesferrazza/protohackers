{
  description = "protohackers";
  outputs = { self, nixpkgs }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    defaultPackage.x86_64-linux = pkgs.rustPlatform.buildRustPackage rec {
      pname = "protohackers";
      version = "0.1.0";
      src = ./.;
      cargoSha256 = "sha256-tWszrX2JsiD2+I2fpPHPq3CuMP2hzCgwSW1d2+B8hqg=";

      meta = with pkgs.lib; {
        description = "My solutions for https://protohackers.com";
        homepage = "https://gitlab.com/kylesferrazza/protohackers";
        license = licenses.mit;
        maintainers = [ maintainers.kylesferrazza ];
      };
    };
    devShell.x86_64-linux = pkgs.mkShell {
      name = "protohackers";
      buildInputs = with pkgs; [
        rustc
        rustfmt
        cargo
        rust-analyzer
        cargo-watch
        clippy
      ];
      RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
    };
  };
}
