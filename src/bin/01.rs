use std::{
    io::{BufRead, Write},
    net::TcpStream,
};

use anyhow::Context;

fn main() {
    protohackers::main(problem_one);
}

fn problem_one(l: &slog::Logger) -> anyhow::Result<()> {
    slog::info!(l, "Prime Time");
    let listener = protohackers::setup_tcp_listener()?;
    protohackers::handle_clients(l, listener, &handle_client);
    Ok(())
}

#[derive(serde::Deserialize, Clone, Debug)]
struct Request {
    method: String,
    number: serde_json::Number,
}

impl Request {
    fn from_json(json_str: &str) -> anyhow::Result<Self> {
        let req: Self = serde_json::from_str(json_str).context("Error parsing JSON")?;
        if req.method != "isPrime" {
            return Err(anyhow::anyhow!(format!(
                "Bad method name: [{}]",
                req.method,
            )));
        }
        Ok(req)
    }
}

#[derive(serde::Serialize, Debug)]
struct Response {
    method: &'static str,
    prime: bool,
}

fn is_prime(num: &serde_json::Number) -> bool {
    if !num.is_u64() {
        return false;
    }
    match num.as_u64() {
        Some(n) => primes::is_prime(n),
        None => false,
    }
}

#[cfg(test)]
mod tests {
    use crate::{is_prime, Request, Response};

    #[test]
    fn test_is_prime() {
        assert!(!is_prime(&47786955.into()));
        assert!(is_prime(&2.into()));
    }

    #[test]
    fn test_response() -> anyhow::Result<()> {
        let req = Request::from_json(
            r#"{
                "method": "isPrime",
                "number": 47786955
            }"#,
        )?;
        let res = Response::new(&req);
        assert!(!res.prime);
        assert_eq!(res.method, "isPrime");
        Ok(())
    }
}

impl Response {
    fn new(req: &Request) -> Self {
        Self {
            method: "isPrime",
            prime: is_prime(&req.number),
        }
    }

    fn to_json(&self) -> anyhow::Result<String> {
        serde_json::to_string(&self).context("Error converting response to JSON string")
    }
}

fn handle_client(l: &slog::Logger, mut stream: TcpStream) -> anyhow::Result<()> {
    let read_stream = stream.try_clone().context("Could not clone stream")?;
    let mut reader = std::io::BufReader::new(&read_stream);
    loop {
        let mut line = String::new();
        reader
            .read_line(&mut line)
            .context("Problem reading line from stream")?;
        if line.is_empty() {
            slog::info!(l, "Got EOF, shutting down");
            break;
        }
        let line = line.trim().to_string();
        slog::info!(l, "Read line: {}", line);

        let req = Request::from_json(&line).context("Malformed request")?;
        slog::info!(l, "Parsed request: {req:?}");
        let res = Response::new(&req);
        slog::info!(l, "Created response: {res:?}");
        let res_json = res.to_json().context("Error converting response to JSON")?;
        writeln!(&mut stream, "{}", res_json).context("Problem writing to stream")?;
    }
    Ok(())
}
