use std::{
    io::{Read, Write},
    net::TcpStream,
};

use anyhow::Context;

fn main() {
    protohackers::main(problem_zero);
}

fn problem_zero(l: &slog::Logger) -> anyhow::Result<()> {
    slog::info!(l, "Smoke test");
    let listener = protohackers::setup_tcp_listener()?;
    protohackers::handle_clients(l, listener, &handle_client);
    Ok(())
}

fn handle_client(l: &slog::Logger, mut stream: TcpStream) -> anyhow::Result<()> {
    loop {
        let mut buf = [0; 1024];
        let num_read = stream
            .read(&mut buf)
            .context("Problem reading from TCP stream")?;
        slog::info!(l, "Received bytes"; "num_read" => num_read);
        if num_read == 0 {
            slog::info!(l, "Got EOF from stream, shutting down");
            break;
        }
        let num_written = stream
            .write(&buf[0..num_read])
            .context("Problem writing to TCP stream")?;
        slog::info!(l, "Wrote bytes back to TCP stream"; "num_written" => num_written);
    }
    Ok(())
}
