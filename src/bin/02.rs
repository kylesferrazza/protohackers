use std::{
    collections::HashMap,
    io::{Read, Write},
    net::TcpStream,
};

use anyhow::Context;

fn main() {
    protohackers::main(problem_two);
}

fn problem_two(l: &slog::Logger) -> anyhow::Result<()> {
    slog::info!(l, "Means to an End");
    let listener = protohackers::setup_tcp_listener()?;
    protohackers::handle_clients(l, listener, &handle_client);
    Ok(())
}

#[derive(PartialEq, Debug)]
struct InsertMessage {
    timestamp: i32,
    price: i32,
}

impl From<([u8; 4], [u8; 4])> for InsertMessage {
    fn from((timestamp, price): ([u8; 4], [u8; 4])) -> Self {
        Self {
            timestamp: i32::from_be_bytes(timestamp),
            price: i32::from_be_bytes(price),
        }
    }
}

#[derive(PartialEq, Debug)]
struct QueryMessage {
    mintime: i32,
    maxtime: i32,
}

impl From<([u8; 4], [u8; 4])> for QueryMessage {
    fn from((mintime, maxtime): ([u8; 4], [u8; 4])) -> Self {
        Self {
            mintime: i32::from_be_bytes(mintime),
            maxtime: i32::from_be_bytes(maxtime),
        }
    }
}

#[derive(PartialEq, Debug)]
enum Message {
    Insert(InsertMessage),
    Query(QueryMessage),
}

impl TryFrom<[u8; 9]> for Message {
    type Error = anyhow::Error;

    fn try_from(slice: [u8; 9]) -> anyhow::Result<Self, Self::Error> {
        let typ: char = slice[0].into();
        let field_one: [u8; 4] = slice[1..=4].try_into()?;
        let field_two: [u8; 4] = slice[5..=8].try_into()?;
        match typ {
            'I' => {
                let msg = (field_one, field_two).into();
                Ok(Message::Insert(msg))
            }
            'Q' => {
                let msg = (field_one, field_two).into();
                Ok(Message::Query(msg))
            }
            _ => Err(anyhow::anyhow!("Bad typ: {}", typ)),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{InsertMessage, Message};

    #[test]
    fn test_parse() -> anyhow::Result<()> {
        let insert_bytes: [u8; 9] = [0x49, 0x00, 0x00, 0x30, 0x39, 0x00, 0x00, 0x00, 0x65];
        let expected = Message::Insert(InsertMessage {
            price: 101,
            timestamp: 12345,
        });
        let msg: Message = Message::try_from(insert_bytes)?;
        assert_eq!(expected, msg);
        Ok(())
    }
}

struct PriceOverTime(HashMap<i32, i64>);

impl PriceOverTime {
    fn insert(self: &mut Self, InsertMessage { timestamp, price }: InsertMessage) {
        self.0.insert(timestamp, price as i64);
    }

    fn query(self: &Self, QueryMessage { mintime, maxtime }: QueryMessage) -> i32 {
        let vals_in_range = self
            .0
            .iter()
            .filter(|&(&timestamp, _)| timestamp >= mintime && timestamp <= maxtime)
            .map(|(_, &price)| price);
        let count: i64 = vals_in_range.clone().count() as i64;
        let sum: i64 = vals_in_range.sum();
        if count == 0 {
            return 0;
        }
        (sum / count) as i32
    }
}

fn handle_client(l: &slog::Logger, mut stream: TcpStream) -> anyhow::Result<()> {
    let mut price_over_time = PriceOverTime(HashMap::new());
    let read_stream = stream.try_clone().context("Could not clone stream")?;
    let mut reader = std::io::BufReader::new(&read_stream);
    loop {
        let mut buf = [0; 9];
        reader
            .read_exact(&mut buf)
            .context("Problem reading 9 bytes from stream")?;
        slog::info!(l, "Read buf: {:02X?}", buf);
        let msg: Message = buf.try_into().context("Could not parse message")?;
        match msg {
            Message::Insert(i) => {
                slog::info!(l, "Insert: {:?}", i);
                price_over_time.insert(i);
            }
            Message::Query(q) => {
                slog::info!(l, "Query: {:?}", q);
                let res = price_over_time.query(q);
                let res_bytes = res.to_be_bytes();
                slog::info!(l, "Answer: {} => {:02X?}", res, res_bytes);
                stream
                    .write_all(&res_bytes)
                    .context("Problem writing answer to stream")?;
            }
        }
    }
}
