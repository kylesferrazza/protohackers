use anyhow::Context;
use slog::Drain;

pub fn setup_logging() -> slog::Logger {
    let drain = slog_term::term_compact().fuse();
    let drain = slog_async::Async::new(drain).build().fuse();
    slog::Logger::root(drain, slog::o!())
}

pub fn listen_address() -> &'static str {
    "0.0.0.0:1337"
}

pub fn setup_tcp_listener() -> anyhow::Result<std::net::TcpListener> {
    std::net::TcpListener::bind(listen_address()).with_context(|| "Problem setting up TCP listener")
}

pub fn handle_clients<F>(
    l: &slog::Logger,
    listener: std::net::TcpListener,
    handle_client: &'static F,
) where
    F: Fn(&slog::Logger, std::net::TcpStream) -> anyhow::Result<()> + std::marker::Sync + 'static,
{
    slog::info!(l, "Waiting for TCP connections");
    let mut stream_num = 0_u32;
    for stream in listener.incoming() {
        let l = l.new(slog::o!("stream_num" => stream_num));
        match stream {
            Ok(stream) => {
                std::thread::spawn(move || {
                    slog::info!(l, "Handling client");
                    if let Ok(addr) = stream.peer_addr() {
                        slog::info!(l, "Got peer address"; "addr" => addr);
                    }
                    if let Err(e) = handle_client(&l, stream) {
                        slog::error!(l, "{:#}", e.context("Error handling stream"));
                    }
                });
                stream_num += 1;
            }
            Err(e) => {
                slog::error!(l, "Problem accepting TCP stream: {:#}", e);
            }
        }
    }
}

pub fn main<F>(entry: F)
where
    F: Fn(&slog::Logger) -> anyhow::Result<()>,
{
    let l = setup_logging();
    if let Err(e) = entry(&l) {
        slog::error!(l, "{:#}", e);
        std::process::exit(1);
    }
}
